export const serverActions = new Set([
  'noop',
  'throw',
  'condition',
  'each',
  'email',
  'log',
  'notify',
  'request',
  'resource.query',
  'static',
]);
