import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  title: 'Documentation',
  search: 'Search',
  reference: 'Reference',
  app: 'App',
  action: 'Action',
  changelog: 'Changelog',
  remapper: 'Remapper',
  packages: 'Packages',
});
