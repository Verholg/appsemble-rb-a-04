import { bootstrap } from '@appsemble/sdk';
import classNames from 'classnames';

function uuid(): string {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    const r = Math.trunc(Math.random() * 16);
    const v = c === 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

bootstrap(({ actions, data, events, parameters: { buttons, vertical }, utils }) => {
  const divName = vertical
    ? 'buttons is-flex is-flex-direction-column is-align-items-center'
    : 'buttons is-centered';

  return (
    <div className={divName}>
      {buttons.map(
        ({
          color,
          fullwidth,
          icon,
          id,
          inverted,
          label,
          light,
          onClick = 'onClick',
          outlined,
          rounded,
          size = 'normal',
          title,
        }) => {
          const action = actions[onClick];
          const node =
            // Accessible content is added below.
            // eslint-disable-next-line jsx-a11y/anchor-has-content
            action?.type === 'link' ? <a href={action.href()} /> : <button type="button" />;
          node.className = classNames('button', `is-${size}`, {
            'is-rounded': rounded,
            'is-fullwidth': fullwidth,
            [`is-${color}`]: color,
            'is-light': light,
            'is-inverted': inverted,
            'is-outlined': outlined,
          });
          node.title = utils.remap(title, data) as string;
          if (icon) {
            node.append(
              <span className="icon">
                <i className={utils.fa(icon)} />
              </span>,
            );
          }

          node.id = id == null ? uuid() : id;

          const createNode = (newData: unknown): ChildNode => {
            const newText = utils.remap(label, newData);
            return typeof newText === 'string' ||
              (typeof newText === 'number' && Number.isFinite(newText)) ? (
              <span>{newText}</span>
            ) : (
              // Even if the label is empty, we want to be able to replace the node when new data is
              // is received.
              document.createTextNode('')
            );
          };
          let currentText = createNode(data);
          let currentData = data;
          node.append(currentText);
          events.on.data((newData) => {
            const newText = createNode(newData);
            currentText.replaceWith(newText);
            currentText = newText;
            currentData = newData;
          });

          node.addEventListener('click', (event) => {
            // Delegate anchor behavior to the link action.
            event.preventDefault();
            action(currentData);
          });
          return node;
        },
      )}
    </div>
  );
});
