import { bootstrap } from '@appsemble/sdk';

import './index.css';

interface SelectedTable {
  tableNumber: number;
  dateFrom: Date;
  dateTo: Date;
}

const isDateInRange = (tableFromDate: Date, tableToDate: Date, selectedDate: Date): boolean => {
  const selected = new Date(selectedDate);
  const from = new Date(tableFromDate);
  const to = new Date(tableToDate);
  return selected >= from && selected <= to;
};

const addHours = (dateFrom: Date, hours: number): Date => {
  const newDate = new Date(dateFrom);
  newDate.setHours(newDate.getHours() + hours);
  return newDate;
};

function isDateValid(dateFrom: Date, dateTo: Date, selectedDate: Date): boolean {
  let selected: Date = selectedDate;
  if (String(selectedDate) === 'Invalid Date') {
    selected = new Date();
  }
  const from = new Date(dateFrom);
  const to = new Date(dateTo);
  return selected >= from && selected <= to;
}

function selectDateTimeFunc(selectDateValue: string): Date {
  return new Date(selectDateValue);
}

function mapTables(selectedDate: Date, tables: string[]): SelectedTable[] {
  const selectedTables: SelectedTable[] = [];
  let dateFrom: Date = selectedDate;
  if (String(selectedDate) === 'Invalid Date') {
    dateFrom = new Date();
  }

  // TO DO think of what we should do with hours
  const dateTo = addHours(dateFrom, 2);

  for (const table of tables) {
    const data: SelectedTable = {
      tableNumber: Number(table),
      dateFrom,
      dateTo,
    };
    selectedTables.push(data);
  }

  return selectedTables;
}

bootstrap(({ actions, events, parameters }) => {
  // Create Container for all table buttons
  const wrapperBtns = document.createElement('div');
  wrapperBtns.classList.add('container');

  // Create DateTime input
  const datetimeInput = document.createElement('input');
  datetimeInput.type = 'datetime-local';
  datetimeInput.id = 'datetime-input';
  datetimeInput.name = 'datetime';
  datetimeInput.classList.add('datetime-input');
  const now = new Date();
  const offsetInMinutes = now.getTimezoneOffset();
  now.setMinutes(now.getMinutes() - offsetInMinutes);
  const dateString = now.toISOString().slice(0, 16);
  datetimeInput.setAttribute('type', 'datetime-local');
  datetimeInput.setAttribute('value', dateString);
  datetimeInput.setAttribute('min', dateString);

  // Create DateTime Label
  const datetimeLabel = document.createElement('label');
  datetimeLabel.htmlFor = 'datetime-input';
  datetimeLabel.textContent = 'Date and Time:';
  datetimeLabel.classList.add('datetime-label');

  // Create dateTime container for the input and label
  const dateTimeContainer = document.createElement('div');
  dateTimeContainer.classList.add('dataTime-container');
  dateTimeContainer.append(datetimeLabel);
  dateTimeContainer.append(datetimeInput);

  // Create confirm button
  const confirmBtn = document.createElement('button');
  confirmBtn.textContent = 'Confirm';
  confirmBtn.classList.add('confirm-btn');

  // Create cancel button
  const cancelBtn = document.createElement('button');
  cancelBtn.textContent = 'Cancel';
  cancelBtn.classList.add('cancel-btn');

  // Create a container for actions buttons (Cancel and Action)
  const btnContainer = document.createElement('div');
  btnContainer.classList.add('btn-container');
  btnContainer.append(confirmBtn);
  btnContainer.append(cancelBtn);
  const wrapperActions = document.createElement('div');
  wrapperActions.append(btnContainer);

  // Create a container for tooltip buttons
  const wrapperToolTip = document.createElement('div');
  wrapperToolTip.classList.add('wrapperToolTip');

  // Availabe, Unavailable, selected squares
  const squareAvailable = document.createElement('div');
  squareAvailable.classList.add('square', 'square-available');

  const squareNotAvailable = document.createElement('div');
  squareNotAvailable.classList.add('square', 'square-not-available');

  const squareSelected = document.createElement('div');
  squareSelected.classList.add('square', 'square-selected');

  // Availabe, Unavailable, selected labels
  const wrapperAvailable = document.createElement('div');
  wrapperAvailable.classList.add('wrapperAvailable');
  wrapperAvailable.textContent = 'Available';

  const wrapperNotAvailable = document.createElement('div');
  wrapperNotAvailable.classList.add('wrapperNotAvailable');
  wrapperNotAvailable.textContent = 'Unavailable';

  const wrapperSelected = document.createElement('div');
  wrapperSelected.classList.add('wrapperSelected');
  wrapperSelected.textContent = 'Selected';

  // Add all tooltips to the container
  wrapperToolTip.append(squareAvailable);
  wrapperToolTip.append(wrapperAvailable);
  wrapperToolTip.append(squareNotAvailable);
  wrapperToolTip.append(wrapperNotAvailable);
  wrapperToolTip.append(squareSelected);
  wrapperToolTip.append(wrapperSelected);

  // Create a main container for all elements
  const allWrappers = document.createElement('div');
  allWrappers.append(dateTimeContainer);
  allWrappers.append(wrapperBtns);
  allWrappers.append(wrapperToolTip);
  allWrappers.append(wrapperActions);

  const availableTables: SelectedTable[] = [];

  const buttons: HTMLButtonElement[] = [];

  for (let i = 1; i <= parameters.tables.length; i += 1) {
    const button = document.createElement('button');
    // TO DO change the content and the title
    button.textContent = String(i);
    button.title = `The size of table ${i} is ${parameters.tables[i - 1].size}`;
    button.id = String(i);
    button.classList.add('my-button');
    buttons.push(button);
    wrapperBtns.append(button);

    events.on.data((data) => {
      const tables = data as SelectedTable[];
      const selectDateTime = selectDateTimeFunc((datetimeInput as HTMLInputElement).value);
      for (const table of tables) {
        if (table.tableNumber === i && isDateValid(table.dateFrom, table.dateTo, selectDateTime)) {
          button.disabled = true;
        }

        availableTables.push({
          tableNumber: table.tableNumber,
          dateFrom: table.dateFrom,
          dateTo: table.dateTo,
        });
      }
    });
  }

  const reservedTables: string[] = [];

  for (const button of buttons) {
    button.addEventListener('click', (event) => {
      event.preventDefault();
      // Remove the previous selections
      reservedTables.length = 0;
      reservedTables.push(button.id);
      button.disabled = true;

      for (const btn of buttons) {
        if (Number(btn.id) !== Number(button.id)) {
          btn.disabled = false;
          const selectDateTime = selectDateTimeFunc((datetimeInput as HTMLInputElement).value);
          if (
            availableTables.some(
              (x) =>
                x.tableNumber === Number(btn.id) &&
                isDateValid(x.dateFrom, x.dateTo, selectDateTime),
            )
          ) {
            btn.disabled = true;
          }
          btn.classList.remove('all-Buttons-disabled');
        }
      }
      button.classList.add('all-Buttons-disabled');
    });
  }

  confirmBtn.addEventListener('click', (event) => {
    event.preventDefault();
    const selectDateTime = selectDateTimeFunc((datetimeInput as HTMLInputElement).value);
    const test = mapTables(selectDateTime, reservedTables);
    const testData = {
      data: test,
    };
    if (testData.data.length > 0) {
      events.emit.data(testData);
      actions.onConfirm(testData.data[0]);
    }
  });

  cancelBtn.addEventListener('click', (event) => {
    event.preventDefault();
    for (const btn of buttons) {
      btn.disabled = false;
      btn.classList.remove('all-Buttons-disabled');
      const selectDateTime = selectDateTimeFunc((datetimeInput as HTMLInputElement).value);
      for (const table of availableTables) {
        if (
          table.tableNumber === Number(btn.id) &&
          isDateValid(table.dateFrom, table.dateTo, selectDateTime)
        ) {
          btn.disabled = true;
        }
      }
    }
  });

  datetimeInput.addEventListener('change', () => {
    const selectedDateTimeString = (datetimeInput as HTMLInputElement).value;
    const selectDateTime = new Date(selectedDateTimeString);
    for (const table of availableTables) {
      const btn = buttons.find((x) => Number(x.id) === table.tableNumber);
      btn.disabled = false;
      btn.classList.remove('all-Buttons-disabled');
      if (isDateInRange(table.dateFrom, table.dateTo, selectDateTime)) {
        btn.disabled = true;
      }
    }
  });

  return allWrappers;
});
