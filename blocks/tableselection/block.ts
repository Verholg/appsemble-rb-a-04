export interface Table {
  /**
   * The size of the button. By default the size is ‘normal’.
   */
  size: number;
}

declare module '@appsemble/sdk' {
  interface Actions {
    /**
     * What happens if the button confirm is clicked.
     */
    onConfirm: never;

    /**
     * What happens if the button cancel is clicked.
     */
    onCancel: never;
  }

  interface EventEmitters {
    /**
     * On what event to listen for incoming data to display.
     */
    data: never;
  }

  interface Parameters {
    tables: Table[];
  }

  interface EventListeners {
    data: never;
  }
}
