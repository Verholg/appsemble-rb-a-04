// `src/index.ts` is the initial entry point of the block source code run. For small blocks this
// often contains the entire logic of the block. Bigger blocks are often split into smaller modules.
import { bootstrap } from '@appsemble/sdk';

// The bootstrap function injects various properties that can be destructured. You can use your
// editor’s autocomplete to see which variables are available.
bootstrap(({ parameters }) => {
  const accordions = parameters.items;

  const accordionContainer = document.createElement('div');
  accordionContainer.classList.add('accordion-container');

  for (const accordion of accordions) {
    const accordionItem = document.createElement('div');
    accordionItem.classList.add('accordion-item');

    const accordionHeader = document.createElement('div');
    accordionHeader.classList.add('accordion-header');

    accordionItem.style.borderBottom = '1px solid black';
    accordionItem.style.padding = '10px';
    accordionItem.style.marginBottom = '5px';

    accordionHeader.style.display = 'flex';
    accordionHeader.style.alignItems = 'center';
    accordionHeader.style.justifyContent = 'space-between';

    const accordionHeaderText = document.createElement('h2');
    accordionHeaderText.classList.add('accordion-header-text');
    accordionHeaderText.style.fontSize = '24px';
    accordionHeaderText.style.fontWeight = 'bold';
    accordionHeaderText.textContent = accordion.header;

    const accordionButtonText = document.createElement('span');
    accordionButtonText.classList.add('accordion-button-text');
    accordionButtonText.style.fontSize = '24px';
    accordionButtonText.style.fontWeight = 'bold';
    accordionButtonText.style.marginLeft = '10px';
    accordionButtonText.style.cursor = 'pointer';
    accordionButtonText.textContent = '-';

    accordionHeader.append(accordionHeaderText);
    accordionHeader.append(accordionButtonText);
    accordionItem.append(accordionHeader);

    const accordionContent = document.createElement('div');
    accordionContent.classList.add('accordion-content');

    const accordionText = document.createElement('p');
    accordionText.textContent = accordion.text;

    accordionContent.append(accordionText);
    accordionItem.append(accordionContent);
    accordionContainer.append(accordionItem);

    accordionButtonText.addEventListener('click', () => {
      accordionContent.classList.toggle('is-hidden');
      accordionButtonText.textContent = accordionContent.classList.contains('is-hidden')
        ? '+'
        : '-';
    });
  }

  return accordionContainer;
});
