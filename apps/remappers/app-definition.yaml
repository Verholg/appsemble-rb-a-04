name: Remappers
description: This app contains every Remapper currently in Appsemble as of (0.20.42)
defaultPage: Introduction

security:
  default:
    role: User
    policy: everyone
  roles:
    User:
      description: user

resources:
  people:
    roles:
      - $public
    schema:
      type: object
      additionalProperties: false
      properties:
        name:
          type: string
        city:
          type: string
        email:
          type: string

pages:
  - name: Introduction
    blocks:
      - type: markdown
        version: 0.20.42
        parameters:
          content: |
            # Welcome
            This is a list of how to use all the existing remappers.
            To see how these remappers work exactly, clone the app or check the app definition.

            Topics:
              - [Strings](/strings)
              - [Objects](/objects)
              - [Arrays](/arrays)
              - [Data](/data)
              - [History](/history)

      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            type: resource.query
            resource: people
            remapAfter:
              - array.map:
                  if:
                    condition:
                      equals:
                        - prop: city
                        - 'Eindhoven'
                    then:
                      object.omit:
                        - City
                    else: null
              - null.strip: null
            onSuccess:
              type: log
        events:
          emit:
            data: peopleInEindhoven
      - type: table
        version: 0.20.42
        parameters:
          fields:
            - label: Name
              value: { prop: name }
        events:
          listen:
            data: peopleInEindhoven

  - name: Strings
    blocks:
      # string.case
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## string.case
      - type: detail-viewer
        version: 0.20.42
        parameters:
          fields:
            - label:
                - static: patrick
                - string.case: upper

      # string.format
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## string.format
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            type: noop
            remapAfter:
              object.from:
                lotteryPrize: 5000
        events:
          emit:
            data: lotteryResults
      - type: detail-viewer
        version: 0.20.42
        parameters:
          fields:
            - value:
                string.format:
                  template: 'You have won €{lotteryAmount} in the lottery!!'
                  values:
                    lotteryAmount: { prop: lotteryPrize }
        events:
          listen:
            data: lotteryResults

      # string.replace
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## string.replace
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            type: noop
            remapBefore: Eindhoven is the best city in the Netherlands
            remapAfter:
              string.replace:
                (best*)\w+: cleanest
        events:
          emit:
            data: stringReplaceData
      - type: detail-viewer
        version: 0.20.42
        parameters:
          fields:
            - value: { root }
        events:
          listen:
            data: stringReplaceData

  - name: Objects
    blocks:
      # object.from
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## object.from
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            remapBefore:
              object.from:
                username: Chris Taub
                email: example@hotmail.com
                addresses:
                  object.from:
                    work:
                      object.from:
                        city: Eindhoven
                        address: Nachtegaallaan 15
                    home:
                      object.from:
                        city: Amsterdam
                        address: Amstel 1
            type: noop
        events:
          emit:
            data: objectFromData
      - type: detail-viewer
        version: 0.20.42
        parameters:
          fields:
            - label: { prop: username }
              value: { prop: email }
            - label: Work address
              value: [{ prop: addresses }, { prop: work }]
            - label: Home address
              value: [{ prop: addresses }, { prop: home }]
        events:
          listen:
            data: objectFromData

      # object.assign
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## object.assign
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            remapBefore:
              object.from:
                title: Weekly Fishing 21
            type: noop
            remapAfter:
              object.assign:
                author: John Doe
        events:
          emit:
            data: objectAssignData
      - type: detail-viewer
        version: 0.20.42
        parameters:
          fields:
            - label: { prop: title }
              value:
                string.format:
                  template: By {author}
                  values:
                    author: { prop: author }
        events:
          listen:
            data: objectAssignData

      # object.omit
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## object.omit
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            remapBefore:
              object.from:
                title: Weekly Fishing 21
                author: John Doe
                content:
                  object.from:
                    introduction: This is the introduction for the new weekly fishing issue
                    paragraph1: ...
                    interview: ...
            type: noop
            remapAfter:
              object.omit:
                - author
                - - content
                  - interview
        events:
          emit:
            data: objectOmitData
      - type: detail-viewer
        version: 0.20.42
        parameters:
          fields:
            - label: { prop: title }
            - value: [{ prop: content }, { prop: introduction }]
        events:
          listen:
            data: objectOmitData

  - name: Arrays
    blocks:
      # array.from
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## array.from
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            remapBefore:
              object.from:
                name: Peter
                occupation: Delivery driver
            type: noop
            remapAfter:
              array.from:
                - root: null
        events:
          emit:
            data: arrayFromData
      - type: table
        version: 0.20.42
        parameters:
          fields:
            - label: Name
              value: { prop: name }
            - label: Occupation
              value: { prop: occupation }
        events:
          listen:
            data: arrayFromData

      # array.map
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## array.map
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            remapBefore:
              - array.from:
                  - object.from:
                      name: Peter
                      occupation: Delivery driver
                  - object.from:
                      name: Otto
                      occupation: Scientist
                  - object.from:
                      name: Harry
                      occupation: CEO
              - array.map:
                  object.omit:
                    - name
            type: noop
        events:
          emit:
            data: arrayMapData1
      - type: table
        version: 0.20.42
        parameters:
          fields:
            - label: Name
              value: { prop: name }
            - label: Occupation
              value: { prop: occupation }
        events:
          listen:
            data: arrayMapData1

      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            remapBefore:
              - array.from:
                  - object.from:
                      name: Peter
                      occupation: Delivery driver
                  - object.from:
                      name: Otto
                      occupation: Scientist
                  - object.from:
                      name: Harry
                      occupation: CEO
              - array.map:
                  if:
                    condition: { equals: [{ prop: occupation }, Scientist] }
                    then:
                      object.from:
                        name: { prop: name }
                        occupation: { prop: occupation }
                    else: null
              - null.strip: null
            type: noop
        events:
          emit:
            data: arrayMapData2
      - type: table
        version: 0.20.42
        parameters:
          fields:
            - label: Name
              value: { prop: name }
            - label: Occupation
              value: { prop: occupation }
        events:
          listen:
            data: arrayMapData2

      ## array.append
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## array.append
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            remapBefore:
              - array.from:
                  - object.from:
                      name: Peter
                      occupation: Delivery driver
                  - object.from:
                      name: Otto
                      occupation: Scientist
                  - object.from:
                      name: Harry
                      occupation: CEO
              - array.append:
                  - object.from:
                      name: James
                      occupation: News reporter
            type: noop
        events:
          emit:
            data: arrayAppendData
      - type: table
        version: 0.20.42
        parameters:
          fields:
            - label: Name
              value: { prop: name }
            - label: Occupation
              value: { prop: occupation }
        events:
          listen:
            data: arrayAppendData

      ## array.omit
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## array.omit
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            remapBefore:
              - array.from:
                  - object.from:
                      name: Peter
                      occupation: Delivery driver
                  - object.from:
                      name: Otto
                      occupation: Scientist
                  - object.from:
                      name: Harry
                      occupation: CEO
                  - object.from:
                      name: James
                      occupation: News reporter
              - array.omit:
                  - 3
            type: log
        events:
          emit:
            data: arrayOmitData1
      - type: table
        version: 0.20.42
        parameters:
          fields:
            - label: Name
              value: { prop: name }
            - label: Occupation
              value: { prop: occupation }
        events:
          listen:
            data: arrayOmitData1
      - type: table
        version: 0.20.42
        events:
          listen:
            data: arrayOmitData1
        parameters:
          fields:
            - label: Name
              value: { prop: name }
            - label: Occupation
              value: { prop: occupation }
            - onClick: removePerson
              button:
                label: Omit
        actions:
          removePerson:
            remapBefore:
              - array.from:
                  - object.from:
                      name: Peter
                      occupation: Delivery driver
                  - object.from:
                      name: Otto
                      occupation: Scientist
                  - object.from:
                      name: Harry
                      occupation: CEO
                  - object.from:
                      name: James
                      occupation: News reporter
              - array.omit:
                  - context: index
            type: log

      ## array.unique
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## array.unique
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            remapBefore:
              - array.from:
                  - object.from:
                      name: Peter
                      occupation: Delivery driver
                      age: 19
                  - object.from:
                      name: Peter
                      occupation: Photographer
                      age: 19
                  - object.from:
                      name: Otto
                      occupation: Scientist
                      age: 50
                  - object.from:
                      name: Harry
                      occupation: CEO
                      age: 20
              - array.unique:
                  object.from:
                    name: { prop: name }
                    age: { prop: age }
            type: noop
        events:
          emit:
            data: arrayUniqueData
      - type: table
        version: 0.20.42
        parameters:
          fields:
            - label: Name
              value: { prop: name }
            - label: Occupation
              value: { prop: occupation }
        events:
          listen:
            data: arrayUniqueData

  - name: Data
    blocks:
      # root
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## root
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            type: resource.query
            resource: people
            query:
              object.from:
                $filter: city eq 'Eindhoven'
            onSuccess:
              remapBefore:
                object.from:
                  name: Residents of Eindhoven
                  people:
                    root: null
              type: noop
              remapAfter:
                - prop: people
        events:
          emit:
            data: rootData
      - type: table
        version: 0.20.42
        parameters:
          fields:
            - label: Name
              value: { prop: name }
            - label: City
              value: { prop: city }
            - label: Email
              value: { prop: email }
        events:
          listen:
            data: rootData

      # history
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## history
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            remapBefore:
              object.from:
                title: Most influential bands of all time
                content: ...
            type: noop # history 0
            onSuccess:
              type: resource.query # history 1
              resource: people
              onSuccess:
                type: noop # history 2
                onSuccess:
                  remapBefore:
                    history: 1
                  type: noop
        events:
          emit:
            data: 1historyData
      - type: detail-viewer
        version: 0.20.42
        parameters:
          fields:
            - label: { prop: title }
              value: { prop: content }
        events:
          listen:
            data: 1historyData

      # app
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## app
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            remapBefore:
              object.from:
                id:
                  app: id
                locale:
                  app: locale
                url:
                  app: url
            type: noop
        events:
          emit:
            data: appData
      - type: detail-viewer
        version: 0.20.42
        parameters:
          fields:
            - value:
                string.format:
                  template: id = {id}
                  values:
                    id: { prop: id }
            - value:
                string.format:
                  template: locale = {locale}
                  values:
                    locale: { prop: locale }
            - value:
                string.format:
                  template: url = {url}
                  values:
                    url: { prop: url }
        events:
          listen:
            data: appData

      # page
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## page
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            type: noop
            remapBefore:
              object.from:
                data:
                  page: data
                url:
                  page: url
        events:
          emit:
            data: pageData
      - type: detail-viewer
        version: 0.20.42
        parameters:
          fields:
            - value:
                string.format:
                  template: data = {data}
                  values:
                    data: { prop: data }
            - value:
                string.format:
                  template: url = {url}
                  values:
                    url: { prop: url }
        events:
          listen:
            data: pageData

      # user
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## user
      - type: html
        version: 0.20.42
        parameters:
          placeholders:
            sub: { user: sub }
            name: { user: name }
            email: { user: email }
            verified: { user: email_verified }
            picture: { user: picture }
            locale: { user: locale }
          content: |
            <b>Sub:</b>
            <br><span data-content="sub"></span>
            <br><b>Name:</b>
            <br><span data-content="name"></span>
            <br><b>Email:</b>
            <br><span data-content="email"></span>
            <br><b>Verified?</b>
            <br><span data-content="verified"></span>
            <br><b>Locale:</b>
            <br><span data-content="locale"></span>
            <br><b>Picture link:</b>
            <br><span data-content="picture"></span>

      # context
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## context
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            remapBefore:
              array.from:
                - object.from:
                    name: John
                    age: 78
                - object.from:
                    name: Peter
                    age: 30
                - object.from:
                    name: Tom
                    age: 17
            type: noop
        events:
          emit:
            data: contextData
      - type: table
        version: 0.20.42
        events:
          listen:
            data: contextData
        parameters:
          fields:
            - label: Name
              value: { prop: name }
              onClick: clickName
            - label: Age
              value: { prop: age }
        actions:
          clickName:
            remapBefore:
              context: index
            type: message
            body: { root }
            dismissable: true
            timeout: 4000

      # translate
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## translate
      - type: detail-viewer
        version: 0.20.42
        parameters:
          fields:
            - label: { translate: weatherTitle }
              value: { translate: weatherBody }

  - name: History
    blocks:
      # history
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## history
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            type: resource.query
            resource: people
            onSuccess:
              remapBefore:
                object.from:
                  name: Disrupting
                  type: data!
              type: noop
              onSuccess:
                remapBefore:
                  history: 1
                type: noop
        events:
          emit:
            data: historyData1
      - type: detail-viewer
        version: 0.20.42
        layout: static
        parameters:
          fields:
            - label: { prop: name }
              value: { prop: type }
        events:
          listen:
            data: historyData1
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            type: resource.query
            resource: people
            remapAfter:
              object.from:
                name: Disrupting
                type: data!
            onSuccess:
              type: noop
              onSuccess:
                remapBefore:
                  history: 1
                type: noop
        events:
          emit:
            data: historyData2
      - type: detail-viewer
        version: 0.20.42
        layout: static
        parameters:
          fields:
            - label: { prop: name }
              value: { prop: type }
        events:
          listen:
            data: historyData2
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            type: resource.query
            resource: people
            onSuccess:
              type: noop
              onSuccess:
                remapBefore:
                  history: 1
                type: noop
        events:
          emit:
            data: historyData3
      - type: table
        version: 0.20.42
        layout: static
        parameters:
          fields:
            - label: Name
              value: { prop: name }
            - label: city
              value: { prop: city }
        events:
          listen:
            data: historyData3

      # from.history
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## from.history
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            type: noop
            remapAfter:
              object.from:
                name: Rolling stones at Amsterdam Arena
                artist: Rolling Stones
                location: Amsterdam Arena
                date: [{ static: 07-07-2022 }, { date.parse: dd-MM-yyyy }]
                price: $120
            onSuccess:
              type: noop
              onSuccess:
                type: resource.query
                resource: people
                remapAfter:
                  object.from:
                    concertDetails:
                      from.history:
                        index: 1
                        props:
                          name: { prop: name }
                          date: { prop: date }
                          attendees: { root: null }
        events:
          emit:
            data: fromHistoryData
      - type: detail-viewer
        version: 0.20.42
        layout: static
        parameters:
          fields:
            - label: Concert details
              value: [{ prop: concertDetails }, { prop: name }]
            - value: [{ prop: concertDetails }, { prop: date }]
        events:
          listen:
            data: fromHistoryData
      - type: table
        version: 0.20.42
        layout: static
        parameters:
          fields:
            - value: [{ prop: concertDetails }, { prop: attendees }]
              repeat:
                - label: Name
                  value: { prop: name }
                - label: city
                  value: { prop: city }
        events:
          listen:
            data: fromHistoryData

      # assign.history
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## assign.history
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            type: resource.count
            resource: people
            remapAfter:
              object.from:
                peopleAmount: { root: null }
            onSuccess:
              type: noop
              onSuccess:
                remapBefore:
                  object.from:
                    name: Rolling stones at Amsterdam Arena
                    artist: Rolling Stones
                    location: Amsterdam Arena
                    date: [{ static: 07-07-2022 }, { date.parse: dd-MM-yyyy }]
                    price: $120
                type: noop
                remapAfter:
                  object.from:
                    concertDetails:
                      assign.history:
                        index: 1
                        props:
                          attendees: { prop: peopleAmount }
        events:
          emit:
            data: assignHistoryData
      - type: markdown
        version: 0.20.42
        parameters:
          content:
            string.format:
              template: |
                <br /> Title: {name}
                <br /> By: {artist}
                <br /> Where: {location}
                <br /> When: {date}
                <br /> Price: {price}
                <br /> Number of people: {attendees}
              values:
                name: [{ prop: concertDetails }, { prop: name }]
                artist: [{ prop: concertDetails }, { prop: artist }]
                location: [{ prop: concertDetails }, { prop: location }]
                date: [{ prop: concertDetails }, { prop: date }]
                price: [{ prop: concertDetails }, { prop: price }]
                attendees: [{ prop: concertDetails }, { prop: attendees }]
        events:
          listen:
            data: assignHistoryData

      # omit.history
      - type: markdown
        version: 0.20.42
        layout: static
        parameters:
          content: |
            * ## omit.history
      - type: data-loader
        version: 0.20.42
        actions:
          onLoad:
            type: noop
            remapAfter:
              object.from:
                name: Rolling stones at Amsterdam Arena
                artist: Rolling Stones
                location: Amsterdam Arena
                date: [{ static: 07-07-2022 }, { date.parse: dd-MM-yyyy }]
                bandPasswords:
                  array.from: [1, 1, 1]
                bankDetailsAttendees:
                  array.from: [1, 1, 1]
            onSuccess:
              type: noop
              onSuccess:
                type: resource.query
                resource: people
                remapAfter:
                  object.from:
                    attendees: { root }
                    concertDetails:
                      omit.history:
                        index: 1
                        keys:
                          - bandPasswords
                          - bankDetailsAttendees
        events:
          emit:
            data: omitHistoryData
      - type: markdown
        version: 0.20.42
        parameters:
          content:
            string.format:
              template: |
                <br /> Title: {name}
                <br /> By: {artist}
                <br /> Where: {location}
                <br /> When: {date}
                <br /> Passwords of band members: {passwords}
                <br /> Bank details attendees: {bankDetails}
              values:
                name: [{ prop: concertDetails }, { prop: name }]
                artist: [{ prop: concertDetails }, { prop: artist }]
                location: [{ prop: concertDetails }, { prop: location }]
                date: [{ prop: concertDetails }, { prop: date }]
                passwords: [{ prop: concertDetails }, { prop: bandPasswords }]
                bankDetails: [{ prop: concertDetails }, { prop: bankDetailsAttendees }]
        events:
          listen:
            data: omitHistoryData
